let express = require('express');
let bodyparser = require('body-parser')

let app = express();


app.use(bodyparser.json())

app.get("/",(req,res)=>{
    res.send("welcome")
})

app.use('/fetch',require('./Routes/fetch'))

app.use('/fetchById',require('./Routes/fetchById'))

app.use('/register',require('./Routes/register'))


app.use('/delete',require('./Routes/delete'))

app.use('/updatarecord',require('./Routes/updataRecord'))

app.listen(3005,(req,res)=>{
    console.log("server running")
})


